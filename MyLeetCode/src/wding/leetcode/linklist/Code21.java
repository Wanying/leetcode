/**
 * @author Wanying
 * @date Aug 5, 2016
 */
package wding.leetcode.linklist;

/**
 * Question:
 * Merge two sorted linked lists and return it as a new list. 
 * The new list should be made by splicing together the nodes of the first two lists.
 */
public class Code21 {

	/**
	 * My Solution
	 */
	
	public ListNode mergeTwoLists(ListNode l1, ListNode l2){
		if(l1==null) return l2;
		if(l2==null) return l1;
		ListNode list = new ListNode(0);
		ListNode head = list;
		
		while((l1!=null)&&(l2!=null)){
			ListNode node = new ListNode(0);
			if(l1.val<l2.val){
				node = l1;
				l1 = l1.next;
			}else{
				node = l2;
				l2=l2.next;
			}
			list.next=node;
			list = list.next;
		}
		if(l1!=null){
			//all the following elements are well linked
			list.next=l1;
		}
		if(l2!=null){
			list.next=l2;
		}
		
		return head.next;
	}
	
	/**
	 * Solution 1:
	 **/
	
	public ListNode mergeTwoLists2(ListNode l1, ListNode l2){
		ListNode head, p;
		if(l1==null) return l2;
		if(l2==null) return l1;
		
		if(l1.val>l2.val){
			p=l2; l2=l2.next;
		}else{
			p=l1;l1=l1.next;
		}
		head=p;
		while((l1!=null)&&(l2!=null)){
			if(l1.val>l2.val){
				p.next=l2;l2=l2.next;
			}else{
				p.next=l1;l1=l1.next;
			}
			p=p.next;
		}
		if(l1==null) p.next=l2;
		if(l2==null) p.next=l1;
		return head;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
