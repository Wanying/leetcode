/**
 * @author Wanying
 * @date Aug 5, 2016
 */
package wding.leetcode.linklist;

/**
 * Question:
 * Given a linked list, determine if it has a cycle in it.
 * 
 * Follow up:
 * Can you solve it without using extra space?
 *
 */
public class Code141 {

	/**
	 * 由于不让用extra space，所以用一个快指针和一个慢指针，快指针一次移动两步，慢指针一次移动一步，只要快指针赶上慢指针证明存在loop
	 */
	
	public boolean hashCycle(ListNode head){
		ListNode fast = head, slow = head;
		while(fast!=null){
			slow=slow.next;
			if(fast.next==null) return false;
			else fast = fast.next.next;
			if(fast==slow) return true;
		}
		return false;
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
