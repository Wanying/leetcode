/**
 * @author Wanying
 * @date Aug 7, 2016
 */
package wding.leetcode.linklist;

/**
 * Question:
 * Write a program to find the node at which the intersection of two singly linked lists begins.
 *
 */
public class Code160 {

	/**
	 * The solution is made of three steps:
	 * (1) Count the length of each list of the two
	 * (2) Init two pointers to the head of the two list and move the longer
	 * one by offset of the difference between two lists' length 
	 * (3) Move two pointers together forward and if they meet, that's is the intersection, 
	 * otherwise, there is no intersection
	 */
	
	private int countLength(ListNode head){
		ListNode node = head;
		int length=0;
		while(node!=null){
			length++;
			node = node.next;
		}
		return length;
	}
	public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
		
		int countA = countLength(headA);
		int countB = countLength(headB);
		
		ListNode itA = headA;
		ListNode itB = headB;
		
		if(countA>countB){
			for(int i=0;i<countA-countB;i++){
				itA = itA.next;
			}
		}else if(countA<countB){
			for(int i=0;i<countB-countA;i++){
				itB=itB.next;
			}
		}
		
		ListNode intersect = null;
		while(itA!=null){
			if(itA==itB){
				intersect = itA;
				break;
			}else{
				itA = itA.next;
				itB = itB.next;
			}
		}
        return intersect;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
