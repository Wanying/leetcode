/**
 * @author Wanying
 * @date Aug 7, 2016
 */
package wding.leetcode.linklist;

/**
 * Question: Sort a linked list in O(n log n) time using constant space complexity.
 *
 */
public class Code148 {
	
	/**
	 * Thought: 
	 * In various sorting algorithms, two are popular with O(nlogn) running time, Merge Sort and Quick Sort
	 * This problem is based on linked list. So Merge Sort is a good fit here
	 * because swapping which is need by quick sort is not easy for a linked list instead of array
	 * */
	
	/**
	 * 解题报告: 就是对一个链表进行归并排序
	 * 主要考察3个知识点:
	 * (1) 归并排序的整体思路
	 * (2) 找到一个链表的中间结点的方法
	 * (3) 合并两个已经排好序的链表为一个新的有序链表
	 * 
	 * 归并算法的基本思想是: 找到链表中的middle结点， 然后递归对前半部分和后半部分分别进行归并排序
	 * 最后对两个已经排好的链表进行merge
	 * */
	
	//寻找中间结点(快慢指针)
	public static ListNode getMiddleOfList(ListNode head){
		ListNode slow = head;
		ListNode fast = head;
		while(fast.next!=null && fast.next.next!=null){
			slow = slow.next;
			fast=fast.next.next;
		}
		return slow;
	}
	
	//合并两个链表
	public static ListNode mergeTwoList(ListNode headA, ListNode headB){
		ListNode dummyNode = new ListNode(0);
		ListNode cur = dummyNode;
		while(headA!=null && headB!=null){
			if(headA.val<=headB.val){
				cur.next=headA;
				headA=headA.next;
			}else{
				cur.next = headB;
				headB = headB.next;
			}
			cur=cur.next;
		}
		
		cur.next=headA==null?headB:headA;
		return dummyNode.next;
	}
	
	//the main entrance
	public static ListNode sortList(ListNode head){
		if(head==null || head.next==null) //递归出口，当只有一个结点就不再递归
			return head;
		
		ListNode middle =getMiddleOfList(head);
		ListNode next = middle.next;
		middle.next=null; //把两个链表断开分为左边(包括middle)和右边
		return mergeTwoList(sortList(head), sortList(next));
	}

}
