/**
 * @author Wanying
 * @date Aug 5, 2016
 */
package wding.leetcode.linklist;

/**
 * Question: Given a link list, remove the nth node from the end of the list and return its head
 *
 *Given linked list: 1->2->3->4->5, and n = 2
 *After removing the second node from the end, the linked list becomes 1->2->3->5.
 */
public class Code19 {

	/**
	 * 采用游标的方式，一个游标s在前面n的位置，另一个p在头部，当s走到链表尾部的时候，就应该是应该删除的地方
	 * 时间复杂度为 O(n)
	 */
	
	public ListNode removeNthFromEnd(ListNode head, int n){
		ListNode temp = new ListNode(0);
		temp.next = head;
		//将两结点置于同一个起始点，即head的前结点，均为空
		ListNode  p = temp;
		ListNode q = temp;
		if(head==null) return null;
		
		/*
		 * p先跑n个结点， 随后跑p,q一起跑
		 * 待p跑到链表尾部时，q结点刚好跑到需要一处的结点上
		 * 然后进行跳过处理即可
		 * */
		
		for(int i=0;i<n;i++){
			p=p.next;
		}
		while(p.next!=null){
			p=p.next;
			q=q.next;
		}
		q.next = q.next.next; //remove a node
		return temp.next;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
