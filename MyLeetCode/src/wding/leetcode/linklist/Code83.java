/**
 * @author Wanying
 * @date Aug 6, 2016
 */
package wding.leetcode.linklist;

/**
 * Question:
 * Given a sorted linked list, delete all duplicates such that each element appear only once.
 * 
 * For example,
 * Given 1->1->2, return 1->2.
 * Given 1->1->2->3->3, return 1->2->3.
 *
 */
public class Code83 {

	/**
	 *思路很简单，逐一比较相邻两个节点的值，当遇到相同的值时，就将后一个节点从链表中移去：
	 */
	
	public ListNode deleteDuplicates(ListNode head){
		
		ListNode dummyHead = head;
		while((head!=null)&&(head.next!=null)){
			if(head.val==head.next.val) head.next=head.next.next;
			else head=head.next;
		}
		return dummyHead;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
