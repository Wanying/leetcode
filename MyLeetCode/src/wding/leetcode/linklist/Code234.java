/**
 * @author Wanying
 * @date Aug 5, 2016
 */
package wding.leetcode.linklist;

/**
 * Question: Given a singly linked list, determine if it is a palindrome.
 *
 */
public class Code234 {

	/**
	 * We can create a new list in reversed order and then compare each node. 
	 * The time and space are O(n).
	 * 解题思路：
	 * 对于数组，判断是否是回文很好办， 只需要用两个指针，从两端向中间扫一下就可以判定
	 * 对于单项链表，首先想到的是将链表复制一份到数组中， 然后用上边的方法就可以了，O(n)时间， O(n)额外空间
	 * 
	 * 由于给定的数据结构是单链表， 要访问链表的尾部元素，必须从头开始遍历。
	 */
	
	public boolean isPalindrome(ListNode head){
		if(head==null) return true;
		
		ListNode p = head;
		//create a new link list
		ListNode prev = new ListNode(head.val);
		
		while(p.next!=null){
			ListNode temp = new ListNode(p.next.val);
			temp.next = prev;
			prev = temp;
			p=p.next;
		}
		
		ListNode p1 = head;
		ListNode p2 = prev;
		
		while(p1!=null){
			if(p1.val!=p2.val) return false;
			
			p1=p1.next;
			p2=p2.next;
		}
		return true;
	}
	
	/**
	 * Follow up:
	 * Could you do it in O(n) time and O(1) space?
	 * You can use a fast and slow pointer to get the center of the list, 
	 * then reverse the second list and compare two sublist
	 * The time is O(n) and space is O(1)
	 * 
	 * */
	
	public boolean isPalindrome2(ListNode head){
		if((head==null)||(head.next==null)){
			return true;
		}
		
		//find the list center
		ListNode fast = head;
		ListNode slow = head;
		
		while((fast.next!=null) && (fast.next.next!=null)){
			fast = fast.next.next;
			slow = slow.next;
		}
		
		ListNode secondHead = slow.next;
		slow.next=null;
		
		//reverse the second part
		ListNode p1 = secondHead;
		ListNode p2 = p1.next;
		
		while((p1!=null)&&(p2!=null)){
			ListNode temp = p2.next;
			p2.next=p1;
			p1=p2;
			p2=temp;
		}
		
		secondHead.next = null;
		
		//compare two sublists now
		ListNode p = (p2==null?p1:p2);
		ListNode q = head;
		while(p!=null){
			if(p.val!=q.val) return false;
			p=p.next;
			q=q.next;
		}
		return true;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
