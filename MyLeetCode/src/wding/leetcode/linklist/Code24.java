/**
 * @author Wanying
 * @date Aug 7, 2016
 */
package wding.leetcode.linklist;

/**
 * Question: Swap Nodes in Pairs
 * 
 * Given a linked list, swap every two adjacent nodes and return its head
 * For example,
 * Given 1->2->3->4, you should return the list as 2->1->4->3.
 * Your algorithm should use only constant space. 
 * You may not modify the values in the list, only nodes itself can be changed.
 * 
 *
 */
public class Code24 {

	/**
	 * 本题考查了基本的链表操作， 注意当改变指针的连接时， 要用一个临时指针指向原来的next值，否则链表丢链，无法找到下一个值
	 * 本题的解题方法:
	 * 需要运用fakehead来指向原来的头指针，防止丢链，用两个指针， ptr1始终指向需要交换的pair的前面一个node， 
	 * ptr2 始终指向需要交换的pair的第一个node
	 * 然后就是进行链表的交换
	 * 需要一个临时指针nextstart, 指向下一个需要交换的pair的第一个node， 保证下一次交换的正确进行
	 * 然后就是正常的链表交换和指针挪动就好
	 * 当链表长度为奇数时，ptr2.next 可能为null
	 * 当链表长度为偶数时，ptr2可能为null
	 * 所以把这两个情况作为终止条件，在while判断就好， 最后返回fakenext， 
	 */
	public ListNode swapPairs(ListNode head){
		
		if(head==null || head.next==null) return head;
		
		ListNode dummyHead = new ListNode(0);
		dummyHead.next=head;
		
		ListNode ptr1 = dummyHead;
		ListNode ptr2 = head;
		
		while(ptr2!=null && ptr2.next!=null){
			ListNode nextStart = ptr2.next.next;
			ptr2.next.next = ptr2;
			ptr1.next = ptr2.next;
			ptr2.next = nextStart;
			ptr1 = ptr2;
			ptr2 = ptr2.next;
		}
		return dummyHead.next;
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
