/**
 * @author Wanying
 * @date Jul 18, 2016
 */
package wding.leetcode.linklist;

/**
 * Question: Insertion Sort List
 *
 * sort a linked list using insertion sort
 */
public class Code147 {
	
	/**
	 * 逻辑简单，代码难写，基础不牢
	 * 链表问题的最重要的就是你那个指针是干啥的
	 * 思路:
	 * 指针的插入排序是一种简单直观的排序算法，它的工作原理是通过构建有序序列，对未排序数据，在已排序序列中从前向后扫描
	 * 找到相应位置并插入。插入排序在实现上，通常采用in-place排序(即只需要用到O(1)的额外空间排序))
	 * 用指针p逐一向后遍历
	 * 0. 申请一个dummyHead结点，其下一个结点指向头结点。如果要在头结点处插入，dummyHead会给我我们带来很大的便利
	 * 1. 当p的值不大于下一个结点值，就进行遍历下一个结点
	 * 2. 当p的值大于下一个结点，那么就将p的下一个结点取出，从前向后扫描，在第一个比它值大的节点处插入该结点
	 * */
	
	public ListNode insertionSortList(ListNode head){
		
		if((head==null)||(head.next==null)) return head;
		ListNode dummyHead = new ListNode(0);//create a head node
		ListNode p = head;
		dummyHead.next=head;//put head node ahead of head 
		
		while(p.next!=null){
			if(p.val<=p.next.val){ //p的值不小于下一个结点
				p=p.next;
			}else{
				ListNode temp = p.next; 
				ListNode q = dummyHead;
				p.next = p.next.next;
				while(q.next.val<temp.val){
					q = q.next;
					temp.next=q.next;
					q.next=temp;
				}
			}
		}
		return dummyHead.next;
	}
}
