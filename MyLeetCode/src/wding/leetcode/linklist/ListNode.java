/**
 * @author Wanying
 * @date Aug 5, 2016
 */
package wding.leetcode.linklist;

/**
 * Question:
 *
 */
public class ListNode{
	
	int val;
	ListNode next;
	
	public ListNode(int x){
		val=x;
		next =null;
	}

}
