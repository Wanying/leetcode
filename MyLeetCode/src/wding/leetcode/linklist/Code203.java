/**
 * @author Wanying
 * @date Aug 7, 2016
 */
package wding.leetcode.linklist;

/**
 * Question: Remove Linked List Elements
 * Remove all elements from a linked list of integers that have value val.
 * Example:
 * Given: 1 --> 2 --> 6 --> 3 --> 4 --> 5 --> 6, val = 6
 * Return: 1 --> 2 --> 3 --> 4 --> 5
 *
 */
public class Code203 {

	/**
	 * 
	 */
	
	public ListNode removeElements(ListNode head, int val) {
	       ListNode dummy = new ListNode(Integer.MAX_VALUE);  
	        dummy.next = head;  
	        ListNode cur = dummy;  
	        while(cur.next != null) {  
	            if(cur.next.val == val)  
	                cur.next = cur.next.next;  
	            else  
	                cur = cur.next;  
	        }  
	        return dummy.next;  
	        
	    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
