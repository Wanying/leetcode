/**
 * @author Wanying
 * @date Aug 6, 2016
 */
package wding.leetcode.linklist;

/**
 * Question:
 * Given a singly linked list, group all odd nodes together followed by the even nodes. 
 * Please note here we are talking about the node number and not the value in the nodes
 * 
 * You should try to do it in place. The program should run in O(1) space complexity 
 * and O(nodes) time complexity. 
 * 
 * Given 1->2->3->4->5->NULL,
 * return 1->3->5->2->4->NULL.
 * 
 * Note:
 * The relative order inside both the even and odd groups should remain as it was in the input. 
 * The first node is considered odd, the second node even and so on ...
 */
public class Code328 {

	/**
	 * My Solution 
	 * Not Work T.T
	 * */
	public ListNode oddEvenList(ListNode head){
		ListNode tail=head;
		while(tail.next!=null){
			tail=tail.next; //a pointer point to the last node
		}
		
		ListNode p = head;
		ListNode q = tail;
		while((p!=tail)&&(p!=tail.next)){
			ListNode even = p.next;
			p.next = p.next.next;
			even.next=null;
			q.next=even;
			q=q.next;
		}
		return head;
	}
	
	/**
	 * 解题思路:
	 * 把奇数连接起来，把偶数连接起来，最后连接奇偶
	 */
	
	public ListNode oddEvenList2(ListNode head){
		if(head==null) return head;
		
		ListNode odd = head;
		ListNode even = head.next;
		ListNode evenHead = head.next;
		while(odd.next!=null && even.next!=null){
			odd.next=even.next;
			even.next = even.next.next;
			odd=odd.next;
			even=even.next;
		}
		odd.next=evenHead;
		return head;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
