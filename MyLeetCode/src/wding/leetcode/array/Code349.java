/**
 * @author Wanying
 * @date Jul 15, 2016
 */
package wding.leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Question:Given two arrays, write a function to compute their intersection.
 * Example:
 * Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2].
 * Note:
 * Each element in the result must be unique.
 * The result can be in any order.
 *
 */
public class Code349 {

	/**
	 * Solution 1: My Solution
	 * use Hashset
	 */
	
	public int[] intersection1(int[] nums1, int[] nums2){
		Set<Integer> result = new HashSet<Integer>();
		
		Set<Integer> set = new HashSet<Integer>();
		
		for(int n=0;n<nums1.length;n++){
			set.add(nums1[n]);
		}
		
		for(int m=0;m<nums2.length;m++){
			if(set.contains(nums2[m])){
				result.add(nums2[m]);
			}
		}
		
		int[] list = new int[result.size()];
		int k=0;
		Iterator<Integer> s = result.iterator();
		while(s.hasNext()){
			list[k]=s.next();
			k++;
		}
		System.out.print("Intersection: "+Arrays.toString(list));
		return list;
	}
	
	/** 
	 * Solution 2: My Solution
	 * Take the functions of Hashset directly 
	 * 
	 * */
	
	public int[] intersection2(int[] nums1, int[] nums2){
		List<Integer> result = new ArrayList<Integer>();
		
		Set<Integer> set = new HashSet<Integer>();
		
		for(int n=0;n<nums1.length;n++){
			set.add(nums1[n]);
		}
		
		for(int m=0;m<nums2.length;m++){
			if(set.contains(nums2[m])){
				result.add(nums2[m]);
				set.remove(nums2[m]);
			}
		}
		
		int[] list = new int[result.size()];
		for(int r=0;r<result.size();r++){
			list[r]=result.get(r);
		}
		System.out.print("Intersection: "+Arrays.toString(list));
		return list;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Code349 code = new Code349();
		int[] nums1 = {1, 2, 2, 1};
		int[] nums2 = {2,2};
		code.intersection2(nums1, nums2);

	}

}
