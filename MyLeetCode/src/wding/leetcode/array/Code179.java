/**
 * @author Wanying
 * @date Jul 17, 2016
 */
package wding.leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Question: Largest Number
 * Given a list of non negative integers, arrange them such that they form the largest number.
 * For example, given [3, 30, 34, 5, 9], the largest formed number is 9534330.
 * 
 * Note: The result may be very large, so you need to return a string instead of an integer.
 * 
 * Credits:
 * Special thanks to @ts for adding this problem and creating all test cases.
 * Subscribe to see which companies asked this question
 */

class Digit implements Comparable{
	
	int num;
	int[] digit;
	
	public Digit(int num, int length){
		this.num = num;
		this.digit = new int[length];
		getDigit();
	}
	private void getDigit(){
		int temp = num;
		int numberLength = String.valueOf(temp).length();
		int k = numberLength-1;
		while(temp>=10){
			int d = temp%10;
			digit[k]=d;
			temp/=10;
			k--;
		}
		digit[k]=temp;
		if(numberLength<digit.length){
			for(int i=numberLength;i<digit.length;i++){
				digit[i]=temp;
			}
		}
	}
	
	public int compareTo(Object o){
		Digit d = (Digit)o;
		int k=0;
		while((k<d.digit.length)&&(d.digit[k]==this.digit[k])){
			k++;
		}
		if(k==digit.length) return 0;
		else if(d.digit[k]>this.digit[k]) return 1;
		else return -1;
		
	}
}
	
public class Code179 {

	/**
	 * Solution 1: 
	 * Hint: Quick Sort to get the descending alphabetic order
	 * This Solution fails at 
	 * Input:
	 * 		[824,938,1399,5607,6973,5703,9609,4398,8247]
	 *      Output:   "9609938824782469735703560743981399"
	 *      Expected: "9609938824824769735703560743981399"
	 */
	
	public String largeNumber(int[] nums){
		int max =-1;
		for(int n=0;n<nums.length;n++){
			if(nums[n]>max){
				max=nums[n];
			}
		}
		int maxLength = String.valueOf(max).length();
		
		List<Digit> list = new ArrayList<Digit>();
		for(int i=0;i<nums.length;i++){
			Digit di = new Digit(nums[i],maxLength);
			list.add(di);
		}
		Collections.sort(list);
		
		if(list.get(0).num==0) return "0";
		StringBuffer sbf = new StringBuffer();
		for(int l=0;l<list.size();l++){
			sbf.append(list.get(l).num);
		}
		
		System.out.println("Largest Number: "+sbf.toString());
		return sbf.toString();
	}
	
	/**
	 * Solution 2:
	 * Others Solution
	 * 先把int数组转为String，然后做一个比较器，
	 * 这里有一个小技巧可以用 (o2 + o1).compareTo(o1 + o2)作为比较器，可以将"大数字"放到前面，然后按照比较器的顺序输出即可，JAVA实现如下
	 * 
	 * 先将数字转换成字符串，然后按照字典序进行排序，这里要重写sort方法里面的compare方法，新建一个比较器
	 * 
	 * beat 48.03% java submission
	 * */
	
	public String largeNumber2(int[] nums){
		String[] str = new String[nums.length];
		for(int i=0;i<nums.length;i++){
			str[i]= String.valueOf(nums[i]);
		}
		
		Comparator<String> comp = new Comparator<String>(){
			public int compare(String o1, String o2){
				return (o2+o1).compareTo(o1+o2);
			}
		};
		
		Arrays.sort(str,comp);
		if (str[0].equals("0"))
	        return "0";
	    StringBuilder sb=new StringBuilder();
	    for (String s : str)
	        sb.append(s);
	    System.out.println("Final Result: "+sb.toString());
	    return sb.toString();
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Code179 code = new Code179();
		int[] nums ={3, 30, 34, 5, 9};
		code.largeNumber2(nums);

	}

}
