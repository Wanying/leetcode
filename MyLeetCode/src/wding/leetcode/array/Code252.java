/**
 * @author Wanying
 * @date Jul 15, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Question:
 * Given an array of meeting time intervals consisting of start and end times 
 * [[s1,e1],[s2,e2],...] (si < ei), determine if a person could attend all meetings.
 * For example,
 * Given [[0, 30],[5, 10],[15, 20]],
 * return false.
 *
 */
public class Code252 {
	
	class Interval{
		int start;
		int end;
		public Interval(int start, int end){
			this.start = start;
			this.end = end;
		}
	}
	
	/**
	 * Solution 1:
	 * My solution, I will use quick sort to rank the array first
	 * Take this chance, I will go over quick sort again
	 * */
	
	public boolean canAttendMeetings(Interval[] intervals){
		
		if(intervals==null) throw new IllegalArgumentException("Intervals is null");
		
		int len = intervals.length;
		if(len<=1) return true;
		
		Comparator<Interval> interval_sort = new Comparator<Interval>(){
			@Override
			public int compare(Interval inter1, Interval inter2){
				return inter1.start-inter2.start;
			}
		};
		
		Arrays.sort(intervals, interval_sort);
		for(int i=1;i<len;i++){
			if(intervals[i-1].end>intervals[i].start) return false;
		}
		return true;

	}
	
	public static void main(String args[]){
		
	}
	
	
	
	
	

}
