/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

import java.util.HashSet;
import java.util.Set;

/**
 * Question:
 * Given an array of integers and an integer k, 
 * find out whether there are two distinct indices i and j in the array such that 
 * nums[i] = nums[j] and the difference between i and j is at most k.
 *
 */
public class Code219 {

	/**
	 * My Solution
	 */
	public boolean containsNearbyDuplicate(int[] nums, int k) {
		 if(nums.length<=1) return false;
	        
	        Set<Integer> st = new HashSet<Integer>();
	        int i=0,j=0;
	        if(nums.length<=k){
	            for(;i<nums.length;i++){
	                st.add(nums[i]);
	            }
	            if(st.size()<nums.length) return true;
	            else return false;
	        }else{
	            for(;j<=k;j++){
	                st.add(nums[j]);
	            }
	            if(st.size()<=k) return true;
	            
	            for(i=0,j=k+1;j<nums.length;i++,j++){
	                st.remove(nums[i]);
	                st.add(nums[j]);
	                if(st.size()<=k) return true;
	            }
	        return false;
	       }
        
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
