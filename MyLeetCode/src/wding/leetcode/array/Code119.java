/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * Question: Pascal's Triangle II
 * Given an index k, return the kth row of the Pascal's triangle
 * For example, given k=3 
 * Return[1,3,3,1]
 * 
 * Note: 
 * Could you optimize your algorithm to use only O(k) extra space? 
 *
 */
public class Code119 {

	/**
	 * 解题思路
	 * 对任意n>0有
	 * f(1,n)=1 (n>0)
	 * f(n,n)=1 (n>2)
	 * f(i,j)=f(i-1,j-1)+f(i,j-1), i>2,j>2
	 * 求第k行
	 */
	
	/*
	 * 与 leetcode: Pascal's Triangle | 
	 * Java最短代码实现 不同的是，本题只需要返回一行，并且要求空间复杂度为超过 O(k)，我们将每一行的结果存放在 result 结果集中，一行一行地更新：
	 * */
	public List<Integer> getRow1(int rowIndex) {  
	    List<Integer> result = new ArrayList<Integer>();  
	    result.add(1);  
	    for (int i = 0; i < rowIndex; i++) {  //进行 rowIndex - 1 次（hang）更新  
	        for (int j = i; j > 0; j--)  //利用本行的结果，更新下一行的结果  
	            result.set(j, result.get(j) + result.get(j - 1));  //下一行的第 j 个元素为上一行的第 j - 1 和第 j 个元素的和  
	        result.add(1);  
	    }  
	    return result;  
	}  
	
	/*
	 * 我们也可以直接根据 rowIndex 推出那一行的结果，而不需要根据前一行进行推导：
	 * */
	public List<Integer> getRow2(int rowIndex) {  
	    List<Integer> ans = new ArrayList();  
	    ans.add(1);  
	    for(int i = 1; i <= rowIndex; i++)  
	        ans.add((int)(Math.round(ans.get(i - 1) * ((double)(rowIndex - i + 1) / i))));  
	    return ans;  
	}  
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
