/**
 * @author Wanying
 * @date Jul 16, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Question: H-Index
 * Given an array of citations (each citation is non-negative integer) of a researcher, write a function 
 * to compute the researcher's h-index
 * 
 * According to the definition of h-index on Wikipedia,
 * "A secientist has index of h if h of his/her N papers have at least h citations, each and the other 
 * N-h papers have no more hthan h citations each"
 * 
 * For example, given citations = [3, 0, 6, 1, 5], which means the researcher has 5 papers in total 
 * and each of them had received 3, 0, 6, 1, 5 citations respectively. 
 * Since the researcher has 3 papers with at least 3 citations each and the remaining two with no more than 3 citations each, 
 * his h-index is 3.
 * Note: If there are several possible values for h, the maximum one is taken as the h-index.
 */
public class Code274 {

	/**
	 * My Solutions
	 * utilize the sort function embedded in Java
	 * Your Runtime beats 4.2% of java submission
	 */
	public int hIndex(int[] citations){
		
		//sort in reverse roder
		if(citations.length==0) return 0;
		Integer[] intArray = new Integer[citations.length];
		for(int c=0;c<intArray.length;c++){
			Integer in = new Integer(citations[c]);
			intArray[c]=in;
		}
		Arrays.sort(intArray,Collections.reverseOrder());
		
		int h;
		for(h=0;h<intArray.length;h++){
			if(intArray[h]<=h) break;
		}
		System.out.println("Citation: "+h);
		return h;
		
	}
	
	/**
	 * Solution 2: 
	 * Reverse Rank
	 * Hint: 
	 * 1. An easy approach is to sort the array first
	 * 2. What are the possible values of h-indexs?
	 * 3. A faster appraoch is to use extra space. 
	 * */
	public int hIndex2(int[] citations){
		/*
		 * 好诡异的思路, 思路是什么
		 * 
		 * */
		
		int h = 0;
		//先排好序，从小到大
        Arrays.sort(citations);
        int l = citations.length;

        for(int i = 0; i < l; i++){

        	if(i + 1 <= citations[l - i - 1]){
        		h = i + 1;
        	}
        }
        System.out.println("H Index: "+h);
        return h;
		
	}
	
	public static void main(String[] args) {
		Code274 code = new Code274();
		int[] citations = {0};
		code.hIndex2(citations);

	}

}
