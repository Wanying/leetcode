/**
 * @author Wanying
 * @date Jul 11, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Question: Two Sum
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution.
 *
 *
 *Solutions:
 *Naive – explore all pairs of two – O(n2)O(n2)
 *Sort and use two pointers – O(nlogn)O(nlog⁡n)
 *Use a hashmap – O(n)O(n), but requires some extra bookkeeping.
 */
public class Code1 {

	/**
	 * Solution 1:
	 * Take the Array Directly
	 * Wrong Reason:
	 * (1) NOT Consider about negative data. 
	 */
	
	public int[] myTwoSum(int[] nums, int target){
		int sum[] = {-1,-1}; //very important, initiation
		for(int i=0;i<nums.length;i++){
			sum[0]=i;
			int remain = target-nums[i];
			for(int j=i+1;j<nums.length;j++){
				if(nums[j]==remain){
					sum[1]=j;
					break;
				}
			}
			if(sum[1]!=-1) break;
		}
		System.out.println("Result: "+Arrays.toString(sum));
		return sum;
	}
	
	/**
	 * Take use of HashMap
	 * Very Fast O(n)
	 * */
	
	public int[] twoSum(int[] nums, int target){
		int[] sum = {-1,-1};
		
		Map<Integer,Integer> numap = new HashMap<Integer,Integer>();
		for(int i=0;i<nums.length;i++){
			if(!numap.containsKey(target-nums[i])){
				numap.put(nums[i], i);
			}else{
				sum[0]=numap.get(target-nums[i]);
				sum[1]=i;
				break;
			}
		}
		System.out.println("Result: "+Arrays.toString(sum));
		return sum;
	}
	
	
	public static void main(String[] args) {
		Code1 code = new Code1();
		int[] nums= {3,2,4};
		int target =6;
		code.twoSum(nums, target);

	}

}
