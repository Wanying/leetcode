/**
 * @author Wanying
 * @date Jul 11, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;

/**
 * Question: Remove Element
 * Given an array and a value, remove all instances of that value in place and return the new length.
 * Do not allocate extra space for another array, you must do this in place with constant memory.
 * The order of elements can be changed. It doesn't matter what you leave beyond the new length.
 * Example:
	Given input array nums = [3,2,2,3], val = 3
	Your function should return length = 2, with the first two elements of nums being 2.
	
	Hint: try two points
 */
public class Code27 {

	/**
	 * Solution 1: My Solution
	 * Two Point: i and j
	 * i for current element
	 * j for the nearest element
	 * if i==val, replace nums[i]==val
	 * 
	 */
	public int myRemoveElement(int[] nums, int val){
		int length = nums.length;
		int i=0;
		int j=0;
		while(i<nums.length){
			if(nums[i]==val){
				for(j=i+1;j<nums.length;j++){
					if(nums[j]!=val){
						nums[i]=nums[j];
						nums[j]=val;
						break;
					}
				}
				if(j>=nums.length){
					break;
				}
			}
			i+=1;
		}
		length=i;
		System.out.println("Result Array: "+Arrays.toString(nums));
		return length;
		
	}
	
	public int removeElement(int[] nums, int val) {
        int index = 0;
        for(int i = 0; i < nums.length; i++)
            if (nums[i] != val) {
                nums[index] = nums[i];
                index++;
            }
        return index;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int nums[]={3,2,2,3};
		int val=3;
		Code27 code = new Code27();
		int len=code.removeElement(nums, val);
		System.out.println("The Result Length is: "+len);

	}

}
