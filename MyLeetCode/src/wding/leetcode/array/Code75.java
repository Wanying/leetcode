/**
 * @author Wanying
 * @date Jul 18, 2016
 */
package wding.leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Question: Sort Colors
 * Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent, with the colors in the order red, white and blue.
 * Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.
 * Note:
 * You are not suppose to use the library's sort function for this problem.
 * click to show follow up.
 * Subscribe to see which companies asked this question
 *
 */
public class Code75 {

	/**
	 * My Solution:
	 * Solution 1:
	 * You beat 78.51% of java submissions
	 */
	
	public void sortColors(int[] nums){
		
		int[] count = new int[3];
		for(int n=0;n<nums.length;n++){
			count[nums[n]]++;
		}
		
		int k=0;
		for(int i=0;i<count[0];i++,k++){
			nums[k]=0;
		}
		for(int i=0;i<count[1];i++,k++){
			nums[k]=1;
		}
		for(int i=0;i<count[2];i++,k++){
			nums[k]=2;
		}
	}
	
	/**
	 * Follow up:
	 * Could you come up with an one-pass algorithm using only constant space? 
	 * My Idea:
	 * 时间复杂度位O(n),空间复杂度为：O(1)
	 * 解题思路:
	 * 将数组nums 分为四段 nums[0,...i] 都为0
	 *               nums[i+1,mid-1]都为1
	 *               nums[mid,...right-1]都为未处理的数据
	 *               nums[right,....length-1] 都为2
	 * */
	
	public void setColors2(int[] nums){
		int left=-1;
		int right = nums.length;
		int mid=0;
		
		while((mid<right)&&(nums[mid]==0)) left=mid++;
		while((mid<right)&&(nums[right-1]==2)) right--;
		while(mid<right){
			while((mid<right)&&(nums[mid]==1)) mid++;
			if(mid==right) break;
			if(nums[mid]==0){
				left+=1;
				int temp = nums[left];
				nums[left]=nums[mid];
				nums[mid]=temp;
				mid++;
			}else if(nums[mid]==2){
				right-=1;
				int temp = nums[right];
				nums[right]=nums[mid];
				nums[mid]=temp;
			}
		}
		System.out.println("Sorted: "+Arrays.toString(nums));
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Code75 code = new Code75();
		int[] nums={0, 1, 2, 0, 1, 1, 2, 2, 0, 1, 2};
		code.setColors2(nums);

	}

}
