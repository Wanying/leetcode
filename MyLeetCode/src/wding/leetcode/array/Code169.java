/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

import java.util.HashMap;
import java.util.Map;

/**
 * Question: Majority Element
 * 
 * Given an array of size n, find the majority element. 
 * The majority element is the element that appears more than [n/2] times
 * You may assume that the array is non-empty and the majority element always exist in the array
 *
 */
public class Code169 {

	/**
	 * 解题思路：
	 * （1）使用HashMap，Map的特点：不允许重复元素，因此在存储前需要判断是否存在
	 * （2）判断HashMap中存在nums[i],如果存在，使用hm.get(nums[i])获取value，即通过key来获得value值，即count(出现的次数)
	 * （3）如果count大于数组长度的一般，即返回该元素
	 * （4）如果count不满足条件，向HashMap存储元素以及出现的次数。
	 */
	public int majorityElement(int[] nums){
		
		Map<Integer,Integer> hm = new HashMap<Integer,Integer>();
		for(int i=0;i<nums.length;i++){
			int count=0;
			if(hm.containsKey(nums[i])){
				count=hm.get(nums[i])+1;
			}else{
				count=1;
			}
			if(count>nums.length/2){
				return nums[i];
			}
			hm.put(nums[i], count);
		}
		return -1;
		
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
