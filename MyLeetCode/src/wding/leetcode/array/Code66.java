/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;

/**
 * Question: Plus One
 * Given a non-negative number represented as an array of digits, plus one to the number 
 * The digits are stored such that the most significant digit is at the head of the list
 *
 */
public class Code66 {

	/**
	 * 从最低位开始数9的个数，找到第一个不为9的地方加1， 如果全为9，则扩展数组
	 */
	
	public int[] plusOne(int[] digits){
		
		int n=digits.length-1;
		for(;n>=0;n--){
			if(digits[n]!=9) break;
		}
		if(n==-1){
			int[] nd = new int[digits.length+1];
			nd[0]=1;
			System.out.println("Final Results: "+Arrays.toString(nd));
			return nd;
		}else{
			digits[n]+=1;
			for(int i=digits.length-1;i>n;i--){
				digits[i]=0;
			}
			System.out.println("Final Results: "+Arrays.toString(digits));
			return digits;
		}
	}
	public static void main(String[] args) {
		Code66 code = new Code66();
		int[] digits={8,9,9,9};
		code.plusOne(digits);

	}

}
