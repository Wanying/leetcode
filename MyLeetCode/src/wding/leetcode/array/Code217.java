/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Question: Contains Duplicate
 * 
 * Given an array of integers, find if the array contains any duplicates. 
 * Your function should return true if any value appear at least twice in the array
 * and it should return false if every element is distinct
 */
public class Code217 {

	/**
	 * Time Limit Exceed!
	 */
	 public boolean containsDuplicate(int[] nums) {
	        if(nums.length<=1) return false;
	        Set<Integer> st = new HashSet<Integer>();
	        for(int i=0;i<nums.length;i++){
	            if(st.contains(nums[i])) return true;
	            else{
	                st.add(nums[i]);
	            }
	        }
	        return false;
	        
	}
	 
	 public boolean containsDuplicate2(int[] nums) {
		 	Arrays.sort(nums);
	        for (int i = 0; i < nums.length - 1; i++) {
	            if (nums[i] == nums[i + 1]) return true;// 循环判断排序后有没有两个相同的数字
	        }
	        return false;
	        
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
