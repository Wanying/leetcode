/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

/**
 * Question: Move Zeros
 * Given an array nums, write a function to move all 0s to the end of it while maintaining 
 * the relative order to the non-zero elements
 * 
 * 
 * For example, given nums = [0, 1, 0, 3, 12], after calling your function, nums should be [1, 3, 12, 0, 0].
 * 
 * Note:
 * You must do this in-place without making a copy of the array.
 * Minimize the total number of operations.
 *
 */
public class Code283 {

	/**
	 * 思路: 维持两个指针
	 */
	
	 public void moveZeroes(int[] nums) {
	        int i=0;
	        int j=0;
	        while(j<nums.length){
	            if(nums[j]!=0){
	                if(j!=i){
	                    nums[i++]=nums[j];
	                    nums[j]=0;
	                }else{
	                    ++i;
	                }
	            }
	            ++j;
	        }
	 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
