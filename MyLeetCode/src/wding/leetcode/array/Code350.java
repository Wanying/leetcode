/**
 * @author Wanying
 * @date Jul 16, 2016
 */
package wding.leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Question:
 *
 */
public class Code350 {

	/**
	 * My Solutioin, slow
	 * Just beat 6.14% of java submission
	 */
	
	public int[] intersect(int[] nums1, int[] nums2){
		 
		Map<Integer,Integer> set1 = new HashMap<Integer,Integer>();
		Map<Integer,Integer> set2 = new HashMap<Integer,Integer>();
		Map<Integer,Integer> finalSet = new HashMap<Integer,Integer>();
		
		if((nums1.length==0)||(nums2.length==0)){
			System.out.println("At Least one put is empty");
			return new int[0];
		}
		
		//put nums1 to set1
		for(int i=0;i<nums1.length;i++){
			if(set1.containsKey(nums1[i])){
				int count = set1.get(nums1[i])+1;
				set1.put(nums1[i], count);
			}else{
				set1.put(nums1[i], 1);
			}
		}
		
		//put nums2 to set2
		for(int j=0;j<nums2.length;j++){
			if(set2.containsKey(nums2[j])){
				int count = set2.get(nums2[j])+1;
				set2.put(nums2[j], count);
			}else{
				set2.put(nums2[j], 1);
			}
		}
		
		//put them to finalset
		Iterator  iter = set1.entrySet().iterator();
		int N=0;
		while(iter.hasNext()){
			Entry<Integer,Integer> entry = (Entry)iter.next();
			int key = entry.getKey();
			int value = entry.getValue();
			if(!(set2.containsKey(key))) continue;
			else {
				int value2 = set2.get(key);
				if(value2<value) value = value2;
			}
			finalSet.put(key, value);
			N+=value;
		}
		
		//put result into array
		int[] result = new int[N];
		int k=0;
		Iterator finalIter = finalSet.entrySet().iterator();
		while(finalIter.hasNext()){
			Entry<Integer,Integer> fe = (Entry) finalIter.next();
			int key = fe.getKey();
			int value =fe.getValue();
			for(int i=0;i<value;i++){
				result[k+i]=key;
			}
			k+=value;
			
		}
		System.out.println("Final Result: "+Arrays.toString(result));
		return result;
	}
	
	/**
	 * Optimized solution
	 * Your runtime beats 32.532% of javasubmissions
	 * */
	
	public int[] intersect2(int[] nums1, int[] nums2){
		
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		for(int i=0;i<nums1.length;i++){
			if(map.containsKey(nums1[i])==false){
				map.put(nums1[i], 1);
			}else{
				map.put(nums1[i], map.get(nums1[i])+1);
			}
		}
		
		List<Integer> resultList = new ArrayList<Integer>();
		for(int i=0;i<nums2.length;i++){
			if(map.containsKey(nums2[i])){
				if(map.get(nums2[i])>0){
					resultList.add(nums2[i]);
					map.put(nums2[i], map.get(nums2[i])-1);
				}
			}
		}
		int result[] = new int[resultList.size()];
        for(int i=0;i<resultList.size();i++)
            result[i]=resultList.get(i);
        System.out.println("Final Result: "+Arrays.toString(result));
        return result;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Code350 code = new Code350();
		int[] nums1 ={1};
		int[] nums2 ={1,1};
		code.intersect2(nums1, nums2);

	}

}
