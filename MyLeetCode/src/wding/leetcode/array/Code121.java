/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

/**
 * Question: Best Time to Buy and Sell Stock
 * 
 * Say you have an array for which the ith element is the price of a given stock on day i
 * If you were only permitted to complete at most one transaction
 *  (i.e. buy one and sell one share of the stock)
 *  Design an algorithm to find the maximum profit
 * 
 *  Example 1:
 *  Input: [7, 1, 5, 3, 6, 4]
 *  Output: 5
 *  max. difference = 6-1 = 5 (not 7-1 = 6, as selling price needs to be larger than buying price)
 *  
 *  Example2: 
 *  Input: [7, 6, 4, 3, 1]
 *  Output: 0
 *  In this case, no transaction is done, i.e. max profit = 0.
 *
 */
public class Code121 {

	/**
	 * 只需要找出最大差值即可
	 * 即max(prices[j]-prices[i]), i<j.
	 * 一次遍历即可，
	 * 
	 * 数组中第i个数字减去前面i-1个数字中的最小数字即为从前i个股票价格变化中可得到的最大收益。
	 */
	 public int maxProfit(int[] prices) {
	        int maxPro = 0;
	        int minPrice = Integer.MAX_VALUE;
	        for (int price : prices) {
	            maxPro = Math.max(maxPro, price - minPrice);
	            minPrice = Math.min(minPrice, price);
	        }
	        return maxPro;
	        
	    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
