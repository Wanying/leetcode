/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

/**
 * Question:
 * Given a sorted array, remove the duplicates in place such that 
 * each element appear only once and return the new length.
 * 
 * Do not allocate extra space for another array, you must do this in place with constant memory.

For example,
Given input array nums = [1,1,2],

Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
 It doesn't matter what you leave beyond the new length.
 *
 */
public class Code26 {

	/**
	 * Use two points 
	 */
	public int removeDuplicates(int[] nums){
		if(nums.length<=1) return nums.length;
		int i=0;
		int temp =nums[0];
		for(int j=1;j<nums.length;j++){
			if(nums[j]!=temp){
				nums[i+1]=nums[j];
				temp=nums[j];
				i++;
			}
		}
		return (i+1);
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
