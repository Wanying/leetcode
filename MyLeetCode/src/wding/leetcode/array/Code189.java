/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

/**
 * Question: Rotate Array
 * Rotate an array of n elements to the right by k steps
 * For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].
 *
 *Note:
 *Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
 */
public class Code189 {

	/**
	 * 小侃:
	 * 起手我没有看懂题目，啥叫做rotate， 啥是k, 3有啥用， 要是看到这个文章是因为文章没有看懂，请默默举起你的爪子
	 * 后来明白了，就是把最后一个数放到第一个上去叫做rotate, e.g: 
	 * 原始: 1234567，一次rotate之后就是7123456, 两次rotate之后就是6712345，k就是rotate的次数
	 * 顺便继续看，要求in-place with O(1)，这就是一个坑爹的要求，不让用多余的数组，给你一个数组，你只能在这里面玩儿，好吧，你赢了
	 * 这时候最笨的方法，就是一次rotate就循环一次喽
	 * 
	 * 说到底，就是k等于几的时候，就把后面几个数挪到最前面，后面的数保持先前的顺序，后面的数也保持先前的顺序， 那么我们接着想如， 拿k=3为例子
	 * 5671234， 所以没有没办法就能一步把567不管顺序先拿到前面去，有对调
	 * 1234567全数组对调(1换7,2换6,3换5)，变成了7654321，然后发现567在数组前面了， 1234在数组后面了，可惜顺序不对
	 * 
	 * 不过没有关系啦，1234567变7654321都做了，765变567就笑case了
	 */
	public void rotate(int[] nums, int k) {  
        k = (nums.length + (k % nums.length)) % nums.length;   
        reverse(nums,0,nums.length - 1);  
        reverse(nums,0,k-1);  
        reverse(nums,k,nums.length - 1);  
    }  
    public void reverse(int[] nums,int start,int end){  
        for (int i = start, j = end; i < j; i++, j--) {  
            int tmp = nums[i];  
            nums[i] = nums[j];  
            nums[j] = tmp;  
        }  
    }  
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
