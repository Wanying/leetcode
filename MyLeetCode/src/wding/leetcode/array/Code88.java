/**
 * @author Wanying
 * @date Aug 7, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;

/**
 * Question:
 * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.
 * 
 * Note: 
 * You may assume that nums1 has enough space (size that is greater or equal to m+n )
 * to hold additional elements from nums2. 
 * The number of elements initialized in nums1 and nums2 are m and n respectively 
 */


/*
 * Two Questions:
 * (1) How these two array sorted? small to large or large to small?
 * (2) Do they sort in the same order
 * */
public class Code88 {

	/**
	 * My Solution
	 * Insert the elements in nums2 to nums1
	 * Fail to Run
	 */
	public void merge(int[] nums1, int m, int[] nums2, int n){
		int j=0;
		int k=m;
		for(int i=0;i<n;i++){
			while(j<m+n){
				if(nums1[j]>nums2[i]){
					int s= k;
					while(s>=j){
						nums1[s+1]=nums1[s];
						s--;
					}
					nums1[j]=nums2[i];
					k++;
					break;
				}else{
					j++;
				}
			}
		}
		System.out.println("Final Result: "+Arrays.toString(nums1));
	}
	
	/**
	 * 小侃:
	 * 两个array, 都是有序的， 然后要求把两个array merge在一起，然后给了一个要求说， nums1有足够的地方可以存放所有的数
	 * 其实也就是说不允许有新的空间， 然后说nums1有m个数，nums2有n个数
	 * 
	 * 既然知道nums1够大，那就是把所有的数放入到这个array中，那么我们一共就有m+n个数放在里面，那么需要m+n-1次循环， 
	 * 然后不能从小到大排，那样会需要新空间， 否则会抹掉值，所以需要从后向前loop。需要两个指针保存两个array的位置。
	 * 然后每次循环呢，茖葱array中取出一个数比较大小，结果就出来了
	 * 
	 * */
	
	public void merge2(int[] nums1, int m, int nums2[], int n){
		int i=m-1;
		int j=n-1;
		for(int k=m+n-1;k>=0;k--){
			if((j<0)||(i>=0 && nums1[i]>=nums2[j])){
				nums1[k]=nums1[i--];
			}else{
				nums1[k]=nums2[j--];
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Code88 code = new Code88();
		int[] nums1= {0,2,0};
		int[] nums2={1};
		int m=1;
		int n=2;
		code.merge(nums1, m, nums2, n);

	}

}
