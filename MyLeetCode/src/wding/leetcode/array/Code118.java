/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * Question: Pascal's Triangle
 * Given numRows, generate the first numRows of Pascal's triangle.
 * For example, given numRows = 5,
 * Return
 *
 */
public class Code118 {

	/**
	 * 杨辉三角形， 帕斯卡三角形
	 * 是二项式系数的一种写法， 形似三角形
	 */
	 public List<List<Integer>> generate(int numRows) {
		 
		 List<List<Integer>> result = new ArrayList<List<Integer>>();
		 if(numRows<=0) return result;
		 List<Integer> pre = new ArrayList<Integer>();
		 pre.add(1);
		 result.add(pre);
		 
		 if(numRows==1) return result;
		 
		 for(int i=2;i<=numRows;i++){
			 List<Integer> row = new ArrayList<Integer>();
			 row.add(1);
			 
			 if(numRows>2){
				 for(int j=1;j<i-1;j++){
					 row.add(pre.get(j-1)+pre.get(j));
				 }
			 }
			 row.add(1);
			 result.add(row);
			 pre=row;
		 }
		 return result;
		 
		 
	        
	 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
