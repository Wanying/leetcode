/**
 * @author Wanying
 * @date Jul 15, 2016
 */
package wding.leetcode.array;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Question:
 * Given two strings s and t, write a function to determine if t is an anagram of s.
 * For example,
 * s = "anagram", t = "nagaram", return true.
 * s = "rat", t = "car", return false.
 *
 */
public class Code242 {
	
	
	/**
	 * Solution 1: take advantage of HashMap
	 * Create a hashmap of each of them, and count their letter count 
	 * */
	public boolean isAnagram(String s, String t){
		if(s.length()!=t.length()){
			System.out.println("Length of s: "+s.length());
			System.out.println("Length of t: "+t.length());
			return false;
		}
		
		Map<String,Integer> sMap = new HashMap<String,Integer>();
		/*
		 * put every letter into sMap
		 * */
		for(int i=0;i<s.length();i++){
			String l = s.substring(i, i+1).trim();
			if(sMap.containsKey(l)){
				int count = sMap.get(l);
				count+=1;
				sMap.put(l, count);
			}else{
				sMap.put(l, 1);
			}
		}
		/*
		 * iterate each letter in t, if sMap contains it, reduce it. 
		 * */
		for(int j=0;j<t.length();j++){
			String l = t.substring(j, j+1).trim();
			//if t contains letters that s does not, t is not an anagram of s
			if(!(sMap.containsKey(l))) {
				return false;
			}
			else{
				int count = sMap.get(l);
				count-=1;
				if(count==0){
					sMap.remove(l);
				}else{
					sMap.put(l, count);
				}
			}
		}
		if(sMap.isEmpty()) return true;
		else{
			return false;
		}
	}
	/**
	 * Solution2:
	 * rank, and then check whether they are the same 
	 * This is much faster than solution 1
	 * */
	
	public boolean isAnagram2(String s, String t){
		char[] chars = s.toCharArray();
		char[] chart = t.toCharArray();
		
		Arrays.sort(chars);
		Arrays.sort(chart);
		
		System.out.println(String.valueOf(chars));

		return String.valueOf(chars).equals(String.valueOf(chart));
	}
	
	
	
	public static void main(String args[]){
		
		Code242 code = new Code242();
		String s="rat";
		String t ="cat";
		boolean is = code.isAnagram2(s, t);
		System.out.println(t+" is an anagram of "+s+" ? "+ is);
	}

}
