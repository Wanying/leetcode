/**
 * @author Wanying
 * @date Aug 8, 2016
 */
package wding.leetcode.array;

/**
 * Question:Product of Array Except Self 
 * 
 * Given an array of n integers where n > 1, nums, 
 * return an array output such that output[i] is equal to the product of all the elements of nums 
 * except nums[i].
 * Solve it without division and in O(n).
 * For example, given [1,2,3,4], return [24,12,8,6].

Follow up:
Could you solve it with constant space complexity? 
(Note: The output array does not count as extra space for the purpose of space complexity analysis.)
 *
 */
public class Code238 {

	/**
	 * 思路: 维持两个数组，left[] 和 right[], 
	 * 维持两个数组, left[] and right[]. 
	 * 分别记录 第i个元素 左边相加的和left[i] and  右边相加的和right[i].  
	 * 那么结果res[i]即为  left[i]+right[i]. 
	 * follow up 要求O(1)空间. 利用返回的结果数组,先存right数组. 再从左边计算left,同时计算结果值, 这样可以不需要额外的空间.
	 * 
	 * 下面是不用除法的方案：
	 * 需要进行两次for循环，第一次循环中把result[i]赋值为前面的数nums的前i-1个数的乘积
	 * 第二次for循环把temp赋值为num的后i-1个数的乘积
	 */
	 public int[] productExceptSelf(int[] nums) {
		 
		 int[] res = new int[nums.length];  
	        res[res.length-1] = 1;  
	        for(int i=nums.length-2; i>=0; i--) {  
	            res[i] = res[i+1] * nums[i+1];  
	        }  
	          
	        int left = 1;  
	        for(int i=0; i<nums.length; i++) {  
	            res[i] *= left;  
	            left *= nums[i];  
	        }  
	        return res;  
	        
	}
	 
	/**
	 * 题目要求返回一个数组，第i个位置的值是在输入数组中，除了i位置的所有数的乘积
	 * 但是要求不能用除法
	 * 
	 * 用除法解决这个问题很简单只需要先把所有数连乘，生成结果的时候再逐一除以一个数
	 * 唯一需要考虑的地方就是数组中的0，
	 * （1）情况一：只要数组中的0的数量大于等于2，那么返回的值一定全为0。
	 * （2）情况二：如果数组中只有一个0，那么返回值除了i位置的数，其他返回值一定也为0。
	 * （3）情况三：输入数组中没有0，简单操作
	 * 
	 * */
	 
	 public int[] productExceptSelf2(int[] nums) {  
	        int product = 1;  
	        int zeros = 0;//记录数组中0的个数  
	        int[] result = new int[nums.length];  
	        for (int num : nums) {  
	            if (num==0) {  
	                zeros++;  
	                if (zeros == 2) { //当数组中有2个以上0时，返回的结果一定全是0  
	                    return result;  
	                }  
	            } else {  
	                product = product * num;  
	            }  
	        }  
	        for (int i = 0; i < nums.length; i++) {  
	            if (zeros == 0) {//数组中没有0的情况  
	                result[i] = product / nums[i];     
	            } else {//数组中有一个0的情况  
	                if (nums[i] == 0) {  
	                    result[i] = product;  
	                }  
	            }  
	        }  
	        return result;  
	    }  
	 
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
