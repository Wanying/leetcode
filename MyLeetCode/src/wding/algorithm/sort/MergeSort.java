/**
 * @author Wanying
 * @date Jul 12, 2016
 */
package wding.algorithm.sort;

import java.util.Arrays;

/**
 * Question:Merge Sort
 *
 */
public class MergeSort {
	
	// merge two ordered array array[first,..mid] and array[mid+1,...last] to new array
	
	public void mergeArray(int array[],int first, int mid, int last, int[] order){
		
		int i=first, m=mid;
		int j=mid+1, n=last;
		int k=0;
		
		while((i<=m)&&(j<=n)){
			if(array[i]<=array[j]){
				order[k++]=array[i++];
			}else{
				order[k++]=array[j++];
			}
		}
		while(i<=m){
			order[k++]=array[i++];
		}
		while(j<=n){
			order[k++]=array[j++];
		}
		//overwrite the array
		for(i=0;i<k;i++){
			array[first+i]=order[i];
		}
	}
	
	public void mergesort(int array[], int first, int last, int[] order){
		if(first<last){
			int mid = (first+last)/2;
			mergesort(array, first,mid,order);
			mergesort(array, mid+1,last,order);
			mergeArray(array,first, mid,last, order);
		}
		System.out.println("After Sort: "+Arrays.toString(array));
	}
	
	public static void main(String args[]){
		MergeSort ms = new MergeSort();
		int array[] = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};  
		ms.mergesort(array, 0, array.length-1, new int[array.length]);
	}
}
