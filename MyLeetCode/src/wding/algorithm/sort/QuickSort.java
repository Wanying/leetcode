/**
 * @author Wanying

 * @date Jul 12, 2016
 */
package wding.algorithm.sort;

import java.util.Arrays;

/**
 * Question: Quick Sort
 * 
 *该方法的基本思想是：
 *1．先从数列中取出一个数作为基准数。
 *2．分区过程，将比这个数大的数全放到它的右边，小于或等于它的数全放到它的左边。
 *3．再对左右区间重复第二步，直到各区间只有一个数。
 *
 *对挖坑填数进行总结
 *1．i =L; j = R; 将基准数挖出形成第一个坑a[i]。
 *2．j--由后向前找比它小的数，找到后挖出此数填前一个坑a[i]中。
 *3．i++由前向后找比它大的数，找到后也挖出此数填到前一个坑a[j]中。
 *4．再重复执行2，3二步，直到i==j，将基准数填入a[i]中。
 */
public class QuickSort {
	
	public int adjustArray(int[] array,int left, int right){
		
		//baseline
		int X = array[left];
		
		while(left<right){
			while((left<right) && (array[right]>=X)) right--;
			if(left<right){
				array[left]=array[right];
				left++;
			}
			
			while((left<right)&&(array[left]<=X)) left++;
			if(left<right){
				array[right]=array[left];
				right--;
			}
		}
		array[left]=X;
		return left;
	}
	
	public int[] quickSort1(int array[], int left, int right){
		
		if(left<right){
			int m = adjustArray(array,left,right);
			quickSort1(array,left,m-1);
			quickSort1(array,m+1,right);
		}
		
		System.out.println("After Sort: "+Arrays.toString(array));
		return array;
	}
	
	
	public int[] quickSort2(int[] array, int left, int right){
		if(left<right){
			int X = array[left];
			int i=left;
			int j=right;
			while(i<j){
				while((i<j)&&(array[j]>=X)) j--;
				if(i<j){
					array[i]=array[j];
					i++;
				}
				
				while((i<j) &&(array[i]<=X)) i++;
				if(left<j){
					array[j]=array[i];
					j++;
				}
			}
			array[left]=X;
			quickSort2(array,left,i-1);
			quickSort2(array,i+1,right);
		}
		System.out.println("After Sort: "+Arrays.toString(array));
		return array;
	}
	
	
	public static void main(String args[]){
		
		int array[] = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};  
		QuickSort qs = new QuickSort();
		qs.quickSort2(array, 0, array.length-1);
		
		
	}

}
