/**
 * @author Wanying
 * @date Jul 12, 2016
 */
package wding.algorithm.sort;

import java.util.Arrays;

/**
 * Question: Shell Sort
 * Shellsort, is an in-place comparison sort. It can be seen as either a generalization of sorting by 
 * exchange (bubble sort) or sorting by insertion (insertion sort). The method starts by sorting pairs
 * of elements far apart from each other, then progressively reducing the gap between elements to be 
 * compared. 
 * 
 * 希尔排序(缩小增量法)属于插入类排序，希尔培训对简单的插入排序进行了简单的该井：
 * 它通过加大插入排序中元素之间的间隔，并在这些间隔的元素中进行插入排序，从而使数据项大跨度地移动
 * 当这些数据项排过一趟序后，希尔排序算法缩小数据项的间隔再进行排序，依次排序下去，进行这些排序时的数据项之间的间隔被称为增量
 * 习惯上，用字母h来表示
 * 
 * 通常h序列有Knuth 提出，该序列从1开始，通过如下公式进行计算
 * h=h*3+1
 * 反过来程序需要反向计算h序列,应该使用
 * h=(h-1)/3
 */
public class ShellSort {
	
	
	public int[] shellSort(int[] array){
		int i,j,gap,k;
		for(gap = array.length/2; gap>0;gap/=2){ //interval
			for(i=0;i<gap;i++){// loop all the elements in one gap			
				for(j=i+gap;j<array.length;j+=gap){
					int temp = array[j];
					for(k=j-gap;k>=0;k-=gap){
						if(array[k]>temp){
							array[k+gap]=array[k];
						}else{
							break;
						}
					}
					array[k+gap]=temp;
				}
			}
			
		}
		System.out.println("After Sort: "+Arrays.toString(array));
		return array;
	}
	
	public int[] shellSort2(int array[]){
		
		int j,k,gap;
		for(gap=array.length/2;gap>0;gap/=2){
			for(j=gap;j<array.length;j++){
				if(array[j]<array[j-gap]){
					int temp = array[j];
					for(k=j-gap;k>=0;k-=gap){
						if(array[k]>temp){
							array[k+gap]=array[k];
						}else{
							break;
						}
					}
					array[k+gap]=temp;
				}
			}
		}
		
		System.out.println("After Sort: "+Arrays.toString(array));
		return array;
	}
	
	
	public static void main(String args[]){
		ShellSort ss = new ShellSort();
		int array[] = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};
		ss.shellSort2(array);
	}

}
