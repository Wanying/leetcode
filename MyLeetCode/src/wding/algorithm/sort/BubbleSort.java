/**
 * @author Wanying
 * @date Jul 12, 2016
 */
package wding.algorithm.sort;

import java.util.Arrays;

/**
 * Question: Bubble Sort (Three Methods)
 * Bubble sort, sometimes referred as sinking sort, is a simple sort algorithm that repeatedly steps through
 * the list to be sorted, compared each pair of adjacent items and swaps them if they are in wrong order. 
 */
public class BubbleSort {

	/**
	 * baseline, sort from small to large
	 */
	public int[] bubbleSort1(int[] array){
		
		int length = array.length;
		int temp;
		for(int i=0;i<length;i++){
			for(int j=1;j<length-i;j++){
				if(array[j-1]>array[j]){
					temp = array[j-1];
					array[j-1] = array[j];
					array[j]=temp;
				}
				
			}
		}
		System.out.println("After Sort: "+Arrays.toString(array));
		return array;
	}
	
	/**
	 * opitimized 1: 
	 * set a flag, if swap happens, flag==true, otherwise==false;
	 * if flag==false, indicating the order is already well sorted, you can break the loop
	 * */
	
	public int[] bubbleSort2(int[] array){
		int length = array.length;
		boolean flag;
		int temp;
		for(int i=0;i<length-1;i++){
			flag=false;
			for(int j=1;j<length;j++){
				if(array[j-1]>array[j]){
					temp = array[j-1];
					array[j-1] = array[j];
					array[j]=temp;
					flag=true;
				}
			}
			if(flag==false) break;
		}
		System.out.println("After Sort: "+Arrays.toString(array));
		return array;
	}
	
	/**
	 * optimized2:
	 * If there is an array with length of 100, and only the first 10 are disordered, and the rest 90 
	 * ordered. 
	 * */
	public int[] bubbleSort3(int[] array){
		
		
		System.out.println("After Sort: "+Arrays.toString(array));
		return array;
	}
	
	public static void main(String[] args) {
		int array[] = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};  
		BubbleSort bs = new BubbleSort();
		bs.bubbleSort1(array);

	}

}
