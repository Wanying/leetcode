/**
 * @author Wanying
 * @date Jul 12, 2016
 */
package wding.algorithm.sort;

import java.util.Arrays;

/**
 * Question:
 * 
 * heapsort在实践中经常被一个实现的很好的快排打败，但heap有另外一个重要的应用，就是Priority Queue。
 * 这篇文章只做拓展内容提及，简单得说，一个priority queue就是一组带key的element，通过key来构造堆结构。通常，
 * priority queue使用的是min-heap，例如按时间顺序处理某些应用中的objects。
 *
 */
public class HeapSort {
	
	/*
	 * build a minmum array
	 * */
	
	private int array[];
	private int heapsize;
	public void buildMaxHeap(int array[]){
		
		this.array=array;
		this.heapsize = array.length;
		
		
		int lastParent = Parent(heapsize-1);
		for(int i=lastParent;i>=0;i--){
			maxHeapify(i);
		}
	}
	
	
	public void maxHeapify(int i){
		// maximize the local tree
		int leftNode = Left(i); //left children
		int rightNode = Right(i); //right children
		int largest =i; //itself
		
		//find the smallest node
		if((leftNode<=heapsize-1)&&(array[leftNode]>array[largest])){
			largest  = leftNode;
		}
		
		if((rightNode<=heapsize-1)&&(array[rightNode]>array[largest])){
			largest = rightNode;
		}
		
		if(largest !=i){
			int temp = array[i];
			array[i]=array[largest];
			array[largest] = temp;
			maxHeapify(largest);
		}
	}
	
	
	public void heapsort(int array[]){
		buildMaxHeap(array);
		System.out.println("Initiation: "+Arrays.toString(array));
		int step=1;
		for(int i=array.length-1;i>0;i--){
			//执行n次，将当前最大放到末尾
			int temp = array[i];
			array[i]=array[0];
			array[0]=temp;
			heapsize--;
			System.out.println("Step: "+(step++)+Arrays.toString(array));
			maxHeapify(0);
		}
		System.out.println("Output: "+Arrays.toString(array));
	}
	
	private int Parent(int i) {
		return (i-1)/2;
	}
	
	private int Left(int i){
		return 2*(i+1)-1;
	}
	private int Right(int i){
		return 2*(i+1);
	}
	
	public static void main(String args[]){
		HeapSort hs = new HeapSort();
		int array[] = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};  
		hs.heapsort(array);
	}

}
