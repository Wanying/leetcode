/**
 * @author Wanying
 * @date Jul 12, 2016
 */
package wding.algorithm.sort;

import java.util.Arrays;

/**
 * Question:
 * Insertion Sort
 * Every to be sorted number will be inserted into a well ordered sequence 
 * rank from small to large
 *
 */
public class InsertionSort {
	
	
	public int[] insertionSort1(int[] array){
		int temp;
		int i,j,k;
		
		for(i=1;i<array.length;i++){
			//get the ith number, and compare it to its previous numbers
			temp = array[i];
			
			for(j=i-1;j>=0;j--){
				if(array[j]<temp) break;
			}
			if(j!=i-1){
				for( k=i-1;k>j;k--){
					array[k+1]=array[k];
				}
				array[k+1]=temp;
			}
		}
		
		System.out.println("Sorted Array: "+Arrays.toString(array));
		return array;
	}
	
	/*
	 * we can compare array[i] to array[i-1]
	 * if array[i]>array[i-1], it is ordered, there is no need to sort
	 * */
	public int[] insertionSort2(int array[]){
		
		for(int i=1;i<array.length;i++){
			if(array[i]>array[i-1]) continue;
			
			int temp = array[i];
			int j;
			for(j=i-1;j>=0;j--){
				if(array[j]>temp){
					array[j+1]=array[j];
				}else{
					break;
				}
			}
			array[j+1]=temp;
		}
		
		
		
		System.out.println("Sorted Array: "+Arrays.toString(array));
		return array;
	}
	
	public static void main(String args[]){
		InsertionSort is = new InsertionSort();
		int array[] = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51}; 
		is.insertionSort2(array);
	}

}
