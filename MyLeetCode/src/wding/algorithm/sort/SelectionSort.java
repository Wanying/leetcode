/**
 * @author Wanying
 * @date Jul 12, 2016
 */
package wding.algorithm.sort;

import java.util.Arrays;

/**
 * Question: Selection Sort
 * The algorithm divides the input list into two parts: the sublist of items already sorted, 
 * which is built up from left to right at the front of the list,
 * and the sublist of items remaining to be sorted that occupy the rest of the list. 
 * Initially, the sorted sublist is empty and the unsorted sublist is the entire input list. 
 * The algorithm proceeds by finding the smallest element in the unsorted sublist, 
 * exchanging it with the leftmost unsorted element, and moving the sublist boudaries one element to 
 * the right
 *
 */
public class SelectionSort {

	/**
	 * @param args
	 */
	
	public int[] selectionSort(int[] array){
		
		for(int i=0;i<array.length-1;i++){
			int min = Integer.MAX_VALUE;
			int minIdx =-1;
			for(int j=i+1;j<array.length;j++){
				if(array[j]<min){
					min = array[j];
					minIdx=j;
				}
			}
			if(array[i]>min){
				//exchange
				int temp = array[i];
				array[i]=array[minIdx];
				array[minIdx]=temp;
			}
		}
		System.out.println("Sorted Array: "+Arrays.toString(array));
		return array;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int array[] = {49,38,65,97,76,13,27,49,78,34,12,64,5,4,62,99,98,54,56,17,18,23,34,15,35,25,53,51};  
		SelectionSort ss = new SelectionSort();
		ss.selectionSort(array);

	}

}
